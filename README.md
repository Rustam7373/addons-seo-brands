# Module: seo brands #

Creating a module to add a brand route (without using the SEO add-on)

### In detail ###

Designed for CMS CS-Cart

### How to install and configure the module? ###

To install, unload the module into the CS-Cart root directory.
